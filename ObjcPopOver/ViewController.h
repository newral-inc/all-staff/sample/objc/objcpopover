//
//  ViewController.h
//  ObjcPopOver
//
//  Created by mfuta1971 on 2019/04/21.
//  Copyright © 2019 paveway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIPopoverControllerDelegate>

@end
