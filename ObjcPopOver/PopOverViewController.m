//
//  PopOverViewController.m
//  ObjcPopOver
//
//  Created by mfuta1971 on 2019/04/21.
//  Copyright © 2019 paveway. All rights reserved.
//

#import "PopOverViewController.h"

@interface PopOverViewController ()

@end

@implementation PopOverViewController{
    UIView *secondView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    secondView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100.0f, 0.0f)];
    secondView.backgroundColor = [UIColor redColor];
    [self.view addSubview:secondView];
}

@end
