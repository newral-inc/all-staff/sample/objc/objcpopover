//
//  ViewController.m
//  ObjcPopOver
//
//  Created by mfuta1971 on 2019/04/21.
//  Copyright © 2019 paveway. All rights reserved.
//

#import "ViewController.h"
#import "PopOverViewController.h"

// 参考
// 【Objective-C】UIPopoverPresentationControllerの実装のやり方【Xcode10.1】
// https://note.mu/iga34engineer/n/n8ecd29f3edfd

//デリゲートの採用はしておく。
@interface ViewController ()<UIPopoverPresentationControllerDelegate>

@end

@implementation ViewController{
    UIButton *nextVCBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initButton];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)initButton{
    nextVCBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height/2 + 50.0f, self.view.bounds.size.width, 10.0f)];
    [nextVCBtn setTitle:@"次のVCに遷移。(UIButton)" forState:UIControlStateNormal];
    [nextVCBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    // ボタンがタッチダウンされた時にnextViewメソッドを呼び出す
    [nextVCBtn addTarget:self action:@selector(nextView:)
        forControlEvents:UIControlEventTouchDown];
    
    [self.view addSubview:nextVCBtn];
}

- (IBAction)nextView:(id)sender {
    //表示したいViewControllerを初期化する。
    PopOverViewController *popOverVC = [[PopOverViewController alloc] init];
    [self presentPopOverWithViewController:popOverVC sourceView:nextVCBtn];
    
}

- (void)presentPopOverWithViewController:(UIViewController *)viewController sourceView:(UIView *)sourceView
{
    viewController.modalPresentationStyle = UIModalPresentationPopover;
    viewController.preferredContentSize = CGSizeMake(100.0, 100.0);
    
    UIPopoverPresentationController *presentationController = viewController.popoverPresentationController;
    presentationController.delegate = self;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    presentationController.sourceView = sourceView;
    presentationController.sourceRect = sourceView.bounds;
    
    [self presentViewController:viewController animated:YES completion:NULL];
}

//iPhoneでの描画に大きく関係するのでしっかり追加しておく
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}
    
@end
